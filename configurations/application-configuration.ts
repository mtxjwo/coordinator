import { ApplicationConfigurationModel } from "../models/application-configuration.model";

export const ApplicationConfiguration = {
  logTimeLocale: "en-GB",
  repositoryProtocol: "https://",
  repositoryParentUrl: "bitbucket.org/project",
  repositoryUsername: "git-user",
  repositoryEmail: "secure@email.com",
  repositoryPassword: "",
  repositories: ["repo1"],
  repositoriesLocalRootPath: "repositories",
  repositoriesLocalPath: ["repo1"],
  repositoriesToPreinstall: ["repo1"],
  repositoriesPreInstallationArgs: ["some args"],
  branchToCheckout: "staging",
  checkRepositoriesChangesEveryMs: 5000,
  timeoutRequestAfterMs: 60000,
} as ApplicationConfigurationModel;
