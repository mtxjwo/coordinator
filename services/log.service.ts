import { ApplicationConfigurationModel } from "../models/application-configuration.model";

export class LogService {
  constructor(protected configurations: ApplicationConfigurationModel) {}

  getLogTime() {
    const configurations = this.configurations;
    const time = new Date();
    return (
      "[" +
      time.toLocaleDateString(configurations.logTimeLocale) +
      " " +
      time.toLocaleTimeString(configurations.logTimeLocale) +
      "]"
    );
  }

  print(message: any) {
    process.stdout.write(this.getLogTime() + ": " + message + "\n");
  }
}
