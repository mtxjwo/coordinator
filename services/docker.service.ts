import * as childProcess from "child_process";

import { Observable } from "rxjs";
export class DockerService {
  constructor(public applicationRootDir: string) {}

  start() {
    return new Observable((observer) => {
      const child = childProcess.spawn("docker-compose", ["up", "-d"]);
      child.stdout.on("data", (data) => {
        console.log("stdout: " + data);
      });
      child.stderr.on("data", (data) => {
        console.log(data);
        observer.next(false);
        observer.complete();
      });
      child.on("error", (err) => {
        console.error("Failed to start subprocess.");
        console.error(err);
      });
      child.on("close", (code) => {
        console.log("child process exited with code " + code);
        observer.next(true);
        observer.complete();
      });
    });
  }

  rebuild() {
    return new Observable((observer) => {
      const child = childProcess.spawn("docker-compose", [
        "up",
        "-d",
        "--build",
      ]);
      child.stdout.on("data", (data) => {
        console.log("stdout: " + data);
      });
      child.stderr.on("data", (data) => {
        console.log(data);
        observer.next(false);
        observer.complete();
      });
      child.on("error", (err) => {
        console.error("Failed to start subprocess.");
        console.error(err);
      });
      child.on("close", (code) => {
        console.log("child process exited with code " + code);
        observer.next(true);
        observer.complete();
      });
    });
  }
}
