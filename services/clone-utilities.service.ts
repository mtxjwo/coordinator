import { ApplicationConfigurationModel } from "../models/application-configuration.model";
import { forkJoin, Observable } from "rxjs";
import * as childProcess from "child_process";
import * as fs from "fs";
import * as fsExtra from "fs-extra";

export class CloneUtilitiesService {
  constructor(protected configurations: ApplicationConfigurationModel) {}

  checkRepositoriesAvailability(repositories: string[]) {
    const rootPath = this.configurations.repositoriesLocalRootPath;
    const toCheckObserve = repositories.map((r) => {
      return this.isRepositoryAvailableLocally([rootPath, r].join("/"));
    });
    return forkJoin(toCheckObserve);
  }

  isRepositoryAvailableLocally(path: string) {
    return new Observable((observer) => {
      fs.access(path, fsExtra.F_OK, (e) => {
        if (e) {
          observer.next(false);
          observer.complete();
          return;
        }
        observer.next(true);
        observer.complete();
        return;
      });
    });
  }

  resovleGitRepositoryAuthorisedUrl(repositoryName: string) {
    return (
      this.configurations.repositoryProtocol +
      this.configurations.repositoryUsername +
      ":" +
      this.configurations.repositoryPassword +
      "@" +
      this.configurations.repositoryParentUrl +
      "/" +
      repositoryName
    );
  }

  preInstallRepository(repositoryName: string) {
    const preInstallationArgs =
      this.configurations.repositoriesPreInstallationArgs[
        this.configurations.repositoriesToPreinstall.indexOf(repositoryName)
      ];
    return new Observable((observer) => {
      const rootPath = this.configurations.repositoriesLocalRootPath;
      const child = childProcess.spawn("bash", [
        rootPath + "/" + repositoryName + "/pre-install.sh",
        preInstallationArgs,
      ]);
      child.stdout.on("data", (data) => {
        console.log("stdout: " + data);
        if (data.indexOf("PRE_INSTALLATION_COMPLETE") > -1) {
          observer.next(true);
          observer.complete();
          return;
        }
      });
      child.stderr.on("data", (data) => {
        console.log("stderr: " + data);
        observer.next(false);
        observer.complete();
      });
      child.on("error", (err) => {
        console.error("Failed to start subprocess.");
        console.error(err);
        observer.next(false);
        observer.complete();
      });
      child.on("close", (code) => {
        console.log("child process exited with code " + code);
        observer.next(true);
        observer.complete();
      });
    });
  }
}
