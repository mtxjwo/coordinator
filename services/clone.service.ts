import { from, of, timer } from "rxjs";
import {
  catchError,
  delayWhen,
  map,
  retryWhen,
  switchMap,
  timeout,
} from "rxjs/operators";
import { ApplicationConfigurationModel } from "../models/application-configuration.model";
import { CloneUtilitiesService } from "./clone-utilities.service";
import { DockerService } from "./docker.service";
import * as gitPromise from "simple-git/promise";
import { LogService } from "./log.service";
import * as mkdirp from "mkdirp";

enum RepositoryResponseEnum {
  NO_UPDATE,
  UPDATE_READY,
  CLONED,
  ERROR_ENCOUNTERED,
  ERROR_ENCOUNTERED_DURING_PRE_INSTALLATION,
  DOCKER_REBUILD,
}

export class CloneService {
  constructor(
    public applicationRootDir: string,
    protected configurations: ApplicationConfigurationModel,
    protected logService: LogService,
    protected cloneUtilitiesService: CloneUtilitiesService = new CloneUtilitiesService(
      configurations
    ),
    protected dockerService: DockerService = new DockerService(
      applicationRootDir
    )
  ) {}

  cloneMissing() {
    const repositories = this.configurations.repositories;
    const repositoriesToPreinstall =
      this.configurations.repositoriesToPreinstall;
    let dockerRebuilding = false;
    let dockerStarted = false;
    return this.cloneUtilitiesService
      .checkRepositoriesAvailability(repositories)
      .pipe(
        switchMap((repositoryStatus) => {
          if (dockerRebuilding === true) {
            throw 1;
          }
          let currentRepositoryToCheck = 0;
          let rebuildDocker = false;
          let skipToPreinstallationStage = false;
          return of(currentRepositoryToCheck).pipe(
            switchMap((i) => {
              if (skipToPreinstallationStage === true) {
                return of(RepositoryResponseEnum.UPDATE_READY);
              }
              if (repositoryStatus[currentRepositoryToCheck] === false) {
                return this.cloneRepository(
                  repositories[currentRepositoryToCheck]
                );
              } else {
                return this.updateRepository(
                  repositories[currentRepositoryToCheck]
                );
              }
            }),
            switchMap((r) => {
              if (
                repositoriesToPreinstall.indexOf(
                  repositories[currentRepositoryToCheck]
                ) <= -1 ||
                r === RepositoryResponseEnum.NO_UPDATE ||
                r === RepositoryResponseEnum.ERROR_ENCOUNTERED
              ) {
                return of(r);
              }
              this.logService.print(
                "Pre-installing repository: " +
                  repositories[currentRepositoryToCheck] +
                  "..."
              );
              return this.cloneUtilitiesService
                .preInstallRepository(repositories[currentRepositoryToCheck])
                .pipe(
                  switchMap((installationResult) => {
                    if (installationResult === false) {
                      return of(
                        RepositoryResponseEnum.ERROR_ENCOUNTERED_DURING_PRE_INSTALLATION
                      );
                    }
                    skipToPreinstallationStage = true;
                    return of(r);
                  })
                );
            }),
            switchMap((r) => {
              if (
                r ===
                RepositoryResponseEnum.ERROR_ENCOUNTERED_DURING_PRE_INSTALLATION
              ) {
                skipToPreinstallationStage = true;
                throw 1;
              }
              if (
                r !== RepositoryResponseEnum.NO_UPDATE &&
                r !== RepositoryResponseEnum.ERROR_ENCOUNTERED
              ) {
                rebuildDocker = true;
              }
              if (r !== RepositoryResponseEnum.ERROR_ENCOUNTERED) {
                currentRepositoryToCheck++;
                if (currentRepositoryToCheck >= repositories.length) {
                  currentRepositoryToCheck = 0;
                  return of(
                    rebuildDocker === true
                      ? RepositoryResponseEnum.DOCKER_REBUILD
                      : RepositoryResponseEnum.NO_UPDATE
                  );
                }
              }
              throw 1;
            }),
            retryWhen((error) =>
              error.pipe(
                map((e) => {
                  if (e !== 1) {
                    this.logService.print(e);
                  }
                }),
                delayWhen((e) => {
                  return timer(1000);
                })
              )
            )
          );
        }),
        switchMap((r) => {
          if (r === RepositoryResponseEnum.DOCKER_REBUILD) {
            this.logService.print("New update assessed, rebuilding Docker...");
            dockerRebuilding = true;
            return this.dockerService.rebuild().pipe(
              switchMap((r) => {
                this.logService.print("Docker rebuild done.");
                dockerRebuilding = false;
                throw 1;
              })
            );
          }
          if (
            r === RepositoryResponseEnum.NO_UPDATE &&
            dockerStarted === false
          ) {
            this.logService.print("Starting docker...");
            return this.dockerService.start().pipe(
              switchMap((r) => {
                this.logService.print("Docker started.");
                dockerStarted = true;
                throw 1;
              })
            );
          }
          throw 1;
        }),
        retryWhen((error) =>
          error.pipe(
            map((e) => {
              if (e !== 1) {
                this.logService.print(e);
              }
            }),
            delayWhen((e) => {
              return timer(this.configurations.checkRepositoriesChangesEveryMs);
            })
          )
        )
      );
  }

  updateRepository(repository: string) {
    const resolvedRepositoryUrl =
      this.cloneUtilitiesService.resovleGitRepositoryAuthorisedUrl(repository);
    const targetBranch = this.configurations.branchToCheckout;
    const cloneDir = this.configurations.repositoriesLocalRootPath;
    return from(
      gitPromise(cloneDir + "/" + repository).pull(
        resolvedRepositoryUrl,
        targetBranch,
        { "--force": true }
      )
    ).pipe(
      switchMap((r) => {
        if (r.summary.changes !== 0) {
          this.logService.print(
            "New update has been automatically applied to repository: " +
              repository
          );
          return of(RepositoryResponseEnum.UPDATE_READY);
        }
        return of(RepositoryResponseEnum.NO_UPDATE);
      }),
      timeout(this.configurations.timeoutRequestAfterMs),
      catchError((e) => {
        this.logService.print(e);
        return of(RepositoryResponseEnum.ERROR_ENCOUNTERED);
      })
    );
  }

  cloneRepository(repository: string) {
    this.logService.print("Cloning repository: " + repository + "...");
    const resolvedRepositoryUrl =
      this.cloneUtilitiesService.resovleGitRepositoryAuthorisedUrl(repository);
    const targetBranch = this.configurations.branchToCheckout;
    const cloneDir = this.configurations.repositoriesLocalRootPath;
    this.logService.print(resolvedRepositoryUrl);
    mkdirp.sync(cloneDir + "/" + repository);
    return from(
      gitPromise(cloneDir).clone(
        resolvedRepositoryUrl + ".git",
        cloneDir + "/" + repository
      )
    ).pipe(
      switchMap((r) => {
        this.logService.print(
          "Cloning repository succeeded for: " + repository
        );
        this.logService.print(
          "Retrieving target branche for: " +
            repository +
            " (" +
            resolvedRepositoryUrl +
            ".git)..."
        );
        return from(
          gitPromise(cloneDir + "/" + repository).pull(
            resolvedRepositoryUrl,
            targetBranch,
            {
              "--force": true,
            }
          )
        );
      }),
      switchMap((r) => {
        this.logService.print(
          "Switching to target branch (" +
            targetBranch +
            ") for: " +
            repository +
            "..."
        );
        return from(
          gitPromise(cloneDir + "/" + repository).checkoutBranch(
            targetBranch,
            "origin/" + targetBranch
          )
        );
      }),
      switchMap((r) => of(RepositoryResponseEnum.CLONED)),
      timeout(this.configurations.timeoutRequestAfterMs),
      catchError((e) => {
        this.logService.print(e);
        return of(RepositoryResponseEnum.ERROR_ENCOUNTERED);
      })
    );
  }
}
