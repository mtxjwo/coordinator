#!/bin/sh
# This is a pre-installation template 
DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# End of script must echo
echo "PRE_INSTALLATION_COMPLETE";
exit 1;