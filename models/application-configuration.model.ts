export interface ApplicationConfigurationModel {
  /**
   * Locale used for log messages
   */
  logTimeLocale: string;

  /**
   * Repository protocol used https or http (https recommended)
   */
  repositoryProtocol: string;

  /**
   * Arguments to be passed on to pre-install.sh during pre-installation of repository
   */
  repositoriesPreInstallationArgs: string[];

  /**
   * The path to parent directory of the repositories i.e. https://bitbucket.org/project
   */
  repositoryParentUrl: string;

  /**
   * The username of the repository user
   */
  repositoryUsername: string;

  /**
   * The e-mail of the repository user
   */
  repositoryEmail: string;

  /**
   * The password of the repository user
   */
  repositoryPassword: string;

  /**
   * Names of the repository that will be cloned from repositoryParentUrl
   */
  repositories: string[];

  /**
   * The root path where the repositories will be cloned
   */
  repositoriesLocalRootPath: string;

  /**
   * Which folder each repositories will be cloned to, relative to the repositoriesLocalRootPath
   */
  repositoriesLocalPath: string[];

  /**
   * Which branch of the repositories will be checked for changes
   */
  branchToCheckout: string;

  /**
   * How frequently should the coordinator check for changes in each repository
   */
  checkRepositoriesChangesEveryMs: number;

  /**
   * Which repositories are to be pre-installed (pre-install.sh is called within the repository)
   */
  repositoriesToPreinstall: string[];

  /**
   * Timeout git request after ms
   */
  timeoutRequestAfterMs: number;
}
