#!/bin/sh
DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
touch $DIR_PATH/coordinator.pid
git update-index --skip-worktree $DIR_PATH/configurations/application-configuration.ts
git update-index --skip-worktree $DIR_PATH/docker-compose.yaml
git update-index --skip-worktree $DIR_PATH/coordinator.pid
git config --global user.email "git@email.co"
git config --global user.name "Username"

apt update
apt-get install -y build-essential
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt install -y dsniff
apt install -y nodejs
apt install -y docker.io
apt install -y docker-compose
apt install -y mysql-server
apt install -y fail2ban
apt install -y cron
iptables -A INPUT -p tcp --dport 22 -j ACCEPT    
iptables -P INPUT ACCEPT

crontab -u root -l | grep -v "0 3 * * * /usr/bin/docker system prune -f" | crontab -u root -
echo "0 3 * * * /usr/bin/docker system prune -f" | crontab

cd $DIR_PATH 
npm i 

echo "Instalation complete.";