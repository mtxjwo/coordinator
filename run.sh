#!/bin/sh
DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

git update-index --skip-worktree $DIR_PATH/configurations/application-configuration.ts
git update-index --skip-worktree $DIR_PATH/docker-compose.yaml
git update-index --skip-worktree $DIR_PATH/coordinator.pid
git config --global user.email "git@mail.co"
git config --global user.name "Username"

screen -S coordinator -dm bash -c "$DIR_PATH/node_modules/.bin/ts-node --transpile-only $DIR_PATH/index.ts" 
echo "Success - coordinator has started at screen coordinator. Enter screen -m -r coordinator to continue.";