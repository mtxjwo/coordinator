import { ApplicationConfiguration } from "./configurations/application-configuration";
import { ApplicationConfigurationModel } from "./models/application-configuration.model";
import { CloneService } from "./services/clone.service";
import { LogService } from "./services/log.service";

export class Coordinator {
  constructor(
    public applicationRootDir = __dirname,
    public configuration: ApplicationConfigurationModel = ApplicationConfiguration,
    public logService = new LogService(configuration),
    protected cloneService: CloneService = new CloneService(
      applicationRootDir,
      configuration,
      logService
    )
  ) {
    this.logService.print("Service has started.");
    this.cloneService.cloneMissing().subscribe();
  }
}

const coordinator = new Coordinator();
